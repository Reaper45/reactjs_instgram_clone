/**
 * Created by joram on 1/9/18.
 */

import './jquery'
import './tether'
import './popper'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
