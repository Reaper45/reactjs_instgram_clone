/**
 * Created by joram on 2/12/18.
 */
import * as $ from 'jquery'

$(document).ready(function() {
  $(window).scroll(function () {
    var height = $(window).scrollTop();
    if (height > 50) {
      $('#navBar').addClass('nav-scrolls')
      $('#stories').addClass('mt-4')
    } else {
      $('#navBar').removeClass('nav-scrolls')
      $('#stories').removeClass('mt-4')
    }
  })
});