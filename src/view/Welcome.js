/**
 * Created by joram on 2/13/18.
 */
import React, {Component} from "react";
import AuthForm from "../components/auth/AuthForm";
import ScreenShotsContainer from '../assets/images/screenshot-container.png'
import Footer from '../components/partials/Footer'
import {connect} from 'react-redux'

class Welcome extends Component {
  constructor (props) {
    super();
    this.state = {}
  }
  render () {
    return (
      <div>
        <div className="container" style={{paddingTop: "1.9rem"}}>
          <div className="row">
            <div className="col pr-0">
              <img style={{marginRight: "-40px"}} src={ScreenShotsContainer} alt="" className="screenshots float-right mt-5"/>
            </div>
            <div className="col">
              <div className="float-left">
                <AuthForm />
              </div>
            </div>
          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return state
}
export default connect(mapStateToProps)(Welcome)