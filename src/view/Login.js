/**
 * Created by joram on 2/25/18.
 */
import React, { Component } from 'react'
import AuthForm from "../components/auth/AuthForm"
import Footer from '../components/partials/Footer'

class Login extends Component {
  render () {
    return (
      <div>
        <div className="pt-5">
          <AuthForm/>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default Login