/**
 * Created by joram on 12/29/17.
 */
import React, {Component} from 'react';
import Post from '../components/Post';
import Stories from "../components/Stories";
import Footer from "../components/partials/FooterSmall";
import {connect} from 'react-redux'

class Home extends Component {
    render() {
        var style = {
            container: {
                padding: "60px 0px"
            }
        }
        return (
            <section className="container insta-container" style={style.container}>
              <div className="row ml-0 mr-0 p-0">
                <div className="col-lg-8 col-md-12">
                  <div className="float-md-right float-sm-none ">
                    <Post extra="Some Extra"/>
                  </div>
                </div>
                <div id="stories" className="stickyStories col-lg-4 pl-0 d-none d-md-block">
                  <div className="text-left">
                    <Stories/>
                  </div>
                  <Footer/>
                </div>
              </div>
            </section>
        );
    }
}
const mapStateToProps = state => {
  return state
}
export default connect(mapStateToProps)(Home)