/**
 * Created by joram on 1/9/18.
 */
import React, {Component} from "react";

class Footer extends Component {
  render () {
    return (
      <div className="footer-small">
        <div className="font-weight-light text-left py-2">
          <a href="">About Us</a> &middot;&nbsp;
          <a href="">Support</a> &middot;&nbsp;
          <a href="">Blog</a> &middot;&nbsp;
          <a href="">Press</a> &middot;&nbsp;
          <a href="">API</a> &middot;&nbsp;
          <a href="">Jobs</a> &middot;&nbsp;
          <a href="">Privacy</a> &middot;&nbsp;
          <a href="">Terms</a> &middot;&nbsp;
          <a href="">Directory</a> &middot;&nbsp;
          <a href="">Profiles</a> &middot;&nbsp;
          <a href="">Language</a>
        </div>
        <p className="font-weight-light text-left text-uppercase">
          &copy; 2018 Instagram-clone
        </p>
      </div>
    );
  }
}

export default Footer;