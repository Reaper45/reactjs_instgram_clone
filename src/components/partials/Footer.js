/**
 * Created by joram on 2/25/18.
 */
import React, {Component} from "react";
import '../../assets/css/Footer.css'

class Footer extends Component {
  render () {
    return (
      <div className="footer container font-weight-bold text-uppercase py-3 px-0">
        <nav className="navbar navbar-expand-lg float-left">
          <ul className="navbar-nav">
            <li className="nav-item">
               <a className="nav-link" href="#">About Us</a>
            </li>
            <li className="nav-item">
               <a className="nav-link" href="#">Support</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Blog</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Press</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">API</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Jobs</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Privacy</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Terms</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Directory</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Profiles</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">Language</a>
            </li>
            </ul>
        </nav>
        <span className="nav-link disabled py-3">&copy; 2018 Instagram-clone</span>
      </div>
    );
  }
}

export default Footer;