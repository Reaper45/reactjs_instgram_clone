/**
 * Created by joram on 1/9/18.
 */
import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import logo from '../../assets/images/instagram.png';
import appname from '../../assets/images/app_name.png';
import SearchBar from "../SearchBar";
import '../../assets/css/NavButton.css';

class NavBar extends Component {
    render () {
        var style = {
            navbar: {
                padding: "15px 0px",
                borderBottom: "solid 1px rgba(0, 0, 0, .0975)"
            }
        }
        const navItems = [
            {icon: "far fa-compass", title: "Explore", link: "/explore"},
            {icon: "far fa-heart", title: "Activities", link: "/activities"},
            {icon: "far fa-user", title: "Profile", link: "/profile"}
        ]
        const NavButton = (props) => {
            var item = props.navItem
            const listItem = (item) => (
                <li className="nav-item">
                    <Link className="nav-button" to={item.link} alt={item.title}>
                        <i className={item.icon}></i>
                        {item.title === 'Activities' && <span className="feed"></span>}
                    </Link>
                </li>
            );
            return listItem(item)
        };
        const ListNavItems = (props) => {
            var items = props.items;
            const listItems = items.map((item, index) =>
                <NavButton key={index} navItem={item}/>
            );
            return (
                <ul className="nav justify-content-end">
                    {listItems}
                </ul>
            );
        }

        const NavBar = (props) => {
            return <div className="container-fluid p-0">
                    <nav id="navBar" className="navbar navbar-expand-lg navbar-light bg-white" style={style.navbar}>
                        <div className="container insta-container">
                            <div className="col text-left pl-0">
                                <Link className="navbar-brand" to="/">
                                    <img src={logo} width="30" height="30" className="d-inline-block align-top" alt="InstaLogo" />
                                    <span className="stroke-container"><span className="stroke"></span></span>
                                    <img className="mt-auto app-name" src={appname} height="40" alt="InstagramClone" />
                                </Link>
                            </div>
                            <div className="col d-none d-md-block">
                                <SearchBar/>
                            </div>
                            <div className="col offset-xs-4 text-right pr-0">
                                <ListNavItems items={navItems} />
                            </div>
                        </div>
                    </nav>
                </div>
        }
        return (
          <NavBar/>
        )
    }
}

export default NavBar;