/**
 * Created by joram on 12/29/17.
 */

import React, { Component } from 'react'
import avatar from '../assets/images/avatar.png'
import {connect} from 'react-redux'
// import { bindActionCreators } from 'redux'
import {fetchPosts} from '../store/actions/post-actions'

class Post extends Component {
  constructor (props) {
    super (props)
    this.onPostComment = this.onPostComment.bind(this)
  }
  componentDidMount() {
      this.props.dispatch(fetchPosts())
  }
  onPostComment(event){
    var comment = event.target.val()
    this.props.onPostComment(comment)
  }
  render () {
    const style = {
      card: {
        maxWidth: "600px",
        margin: "auto auto 60px auto"
      },
      cardTitle: {
        verticalAlign: "middle"
      }
    }
    const unitTimeConvert = (time) => {
      var formatedTime = "3 Hours Ago";
      return formatedTime;
    }
    const ListPosts = () => {
      const posts = this.props.posts
      const listPosts = posts.map((post) =>
        <div className="card text-left bg-white rounded" style={style.card}>
          <div className="card-header bg-white">
            <img className="rounded-circle has-story post-profile-img" src={avatar} alt="profile"/>
            <div className="d-inline-block pl-3" style={style.cardTitle}>
              <a href="#" className="user-handle"><strong>{post.user.username}</strong></a>
              <span className="d-block">{post.location}</span>
            </div>
          </div>
          <img className="card-img img-responsive post-image rounded-0" src={post.images.standard_resolution.url} alt="New Post"/>
          <div className="card-text">
          </div>
          <div className="card-footer bg-white pt-0">
            {/*Action section*/}
            <div className="actions pb-2">
              <a href="" className="action"><i className="far fa-heart p-2"></i></a>
              <a href="" className="action"><i className="far fa-comment p-2"></i></a>
              <a href="" className="action pt-1"><i className="far fa-bookmark pull-right float-right p-2"></i></a>
            </div>
            <div className="py-1">
              <strong>{post.caption.from.username}</strong>&nbsp; <span>{post.caption.text}</span>
            </div>
            {/*Time of post*/}
            <div className="pb-1">
              <time className="text-uppercase text-secondary small" title="Jan 16, 2018">
                {unitTimeConvert(post.created_time)}
              </time>
            </div>
            {/*Comments section*/}
            <form action="" className="horizontal-form comment-form">
              <div className="form-group mb-0 pt-3">
                <input className="border-0 p-1 bg-white" type="text" name="" onChange={this.onPostComment} placeholder="Add a comment..."/><span className=""></span>
              </div>
            </form>
          </div>
        </div>
      );
      return listPosts;
    }
    return (
      <div>
       <ListPosts/>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  console.log(state)
  return {
    posts: state.posts,
    extraProps: props.extra
  }
}
export default connect(mapStateToProps)(Post);