/**
 * Created by joram on 1/17/18.
 */

import React, {Component} from 'react'
import avatar from '../assets/images/avatar.png'

class Stories extends Component {
  render () {
    const StoryContainer = (prop) => {
      const content = prop.content;
      return (
        <div className="story px-2 py-1">
          {/*<div className="has-story">*/}
          {/*</div>*/}
          <img src={content.src} alt={content.handle} className="rounded-circle"/>
          <div className="d-inline-block pl-3" style={style.alignWithImage}>
            <span className="handle text-dark font-weight-bold">{content.handle}</span>
            <br/>
            <span className="user-name text-secondary font-weight-light">{content.extra}</span>
          </div>
        </div>);
    }
    const ListStories = (props) => {
      const stories = props.stories;
      const listStories = stories.map((story)=>
          <StoryContainer content={story} />
        );
      return listStories;
    }
    const style = {
      alignWithImage: {
        position: "relative",
        top: ".2rem"
      },
      hrMargin: {
        marginTop: "0px"
      },
      storiesContainer: {
        borderBottom: "solid 1px rgba(0,0,0,0.1)"
      }
    }
    const userProfile = {
      handle: "_Joramm",
      extra: "Joram Mwashighadi Jr.",
      src: avatar
    }
    const stories = [
      { handle: "betty.mulei", extra: "1 Hours ago", src: avatar },
      { handle: "cynthia.mwadilo", extra: "2 Hours ago", src: avatar },
      { handle: "ruth", extra: "3 Hours ago", src: avatar },
      { handle: "avocoder", extra: "5 Hours ago", src: avatar },
      { handle: "lifeinmombasa", extra: "5 Hours ago", src: avatar }
      ]
    return (
      <div>
        <StoryContainer content={userProfile}/>
        <hr style={style.hrMargin}/>

        <div className="header font-weight-bold pb-1">
          <span className="text-secondary">Stories</span>
          <span className="text-light text-dark float-right">Watch All</span>
        </div>
        <div className="stories" style={style.storiesContainer}>
          {/* List Stories here */}
          <ListStories stories={stories}/>
        </div>
      </div>
    )
  }
}

export default Stories