/**
 * Created by joram on 2/26/18.
 */
import React, {Component} from "react"
import logo from '../../assets/images/app_name.png'
import appleStoreImg from '../../assets/images/applestore.png'
import googlePlayImg from '../../assets/images/googlestrore.png'
import '../../assets/css/Auth.css'
import * as $ from 'jquery'
import {login, signup} from "../../store/actions/user-actions";
import {connect} from 'react-redux'

class AuthForm extends Component {
  constructor (props) {
    super(props);
    this.state = {
      signup: true
    }
    this.changeForm = this.changeForm.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    // this.onLogin = this.onLogin.bind(this)
  }

  changeForm(e){
    e.preventDefault()
    this.setState({signup: !this.state.signup})
  }

  onSignup (form) {
    const formData = {
      email: $('[name="signUpForm_email"]').val(),
      password: $('[name="signUpForm_password"]').val(),
      username: $('[name="signUpForm_username"]').val(),
      full_name: $('[name="signUpForm_full_name"]').val(),
    }
    this.props.dispatch(signup(formData))
  }

  onLogin (form) {
    const formData = {
      username: $('[name="loginForm_username"]').val(),
      password: $('[name="loginForm_password"]').val(),
    }
    this.props.dispatch(login(formData))
  }

  handleSubmit (event) {
    event.preventDefault()
    if (this.state.signup){
      this.onSignup()
    } else {
      this.onLogin()
    }
  }

  render () {
    const style = {
      max_width: {maxWidth: "268px"},
      login: {paddingTop: "5rem"}
    }
    // Inputs
    const FormInputs = () => {
      var signup = this.state.signup
      const Inputs = [
        {describedby: "emeilHelp", name: "loginForm_username", type: "text", id: "email", placeholder: "Phone number, username or email"},
        {describedby: "textHelp", name: "loginForm_password", type: "password", id: "password", placeholder: "Password"}
      ]
      const SinginInputs = [
        {describedby: "emeilHelp", name: "signUpForm_email", type: "text", id: "email", placeholder: "Mobile number or email"},
        {describedby: "textHelp", name: "signUpForm_password", type: "password", id: "password", placeholder: "Password"},
        {describedby: "textHelp", name: "signUpForm_username", type: "text", id: "username", placeholder: "Username"},
        {describedby: "textHelp", name: "signUpForm_full_name", type: "text", id: "full_name", placeholder: "Full Name"}
      ]
      const Input = (props) => {
        var input = props.input
        return (
          <div className="form-group mb-1">
            <input type={input.type} className="form-control" id={input.id} aria-describedby={input.describedby}
                   placeholder={input.placeholder} name={input.name} />
          </div>)
      }
      return signup ? SinginInputs.map((input, index) => <Input input={input}/>) :
        Inputs.map((input, index) => <Input input={input}/>)
    }

    // Signup Titles
    const SignupTitle = () => {
      return (
        <div>
          <p className="text-center text-uppercase text-secondary font-weight-bold">Sign up to see photos and videos<br/> from your friends.</p>
          <a href="" className="btn btn-sm btn-primary w-100 text-center" style={style.max_width}>
            <i className="fab fa-facebook-square mr-3"></i>
            Login With facebook</a>
          <div className="py-3 text-secondary" style={{...style.max_width, margin: "0 auto"}}>
            <span className="hr-line float-left"></span>
            <span className="font-weight-bold">OR</span>
            <span className="hr-line float-right"></span>
          </div>
        </div>
      )
    }

    // Form Bottom
    const FormBottom = () => {
      var signup = this.state.signup
      return (
        <div>
          <div className="auth-bottom px-3 py-4">
            <p className="mb-0">
              {signup ?
                (<span>Have an account?<a className="changeAuth" href="accounts/login" onClick={this.changeForm}>&nbsp;Login</a></span>) :
                (<span>Don't have an account? <a className="changeAuth" href="/" onClick={this.changeForm}>&nbsp;Sign up</a></span>)
              }
            </p>
          </div>
          {/* App store link */}
          <div className="text-center p-3">
            <p className="">Get the app.</p>
            <div>
              <a href="#" className="d-inline"><img className="w-50 p-2" src={appleStoreImg} alt="Apple Store"/></a>
              <a href="#" className="d-inline"><img className="w-50 p-2" src={googlePlayImg} alt="Google Play Store"/></a>
            </div>
          </div>
        </div>
      )
    }
    // Base Auth Form
    const AuthForm = () => {
      const signup = this.state.signup
      return (
        <div className="pt-2">
          <div className="auth-container mb-0 mx-auto" style={!signup? style.login : null}>
            <div className="auth-top p-4 mb-2">
              <img className="text-center w-75 mb-3" src={logo} alt="Instagram Clone"/>
              {signup ? (<SignupTitle/>) : null}
              <form action="" id="authForm" onSubmit={this.handleSubmit}>
                <FormInputs />
                <button type="submit" form="authForm" style={style.max_width} className="btn btn-sm btn-primary w-100 my-2">
                  {signup ? 'Sign up' : 'Login'}</button>
              </form>
              {signup ?
                (<p className="text-center text-secondary my-2">By signing up, you agree to our <br/>
                <a href="" className="font-weight-bold text-muted">Terms & Privacy Policy.</a></p>) :
                <a className="pt-2" href="#"><small id="emailHelp" className="form-text text-muted">Forgot password?</small></a>
              }
            </div>
            <FormBottom/>
          </div>
        </div>
      )
    }

    return <AuthForm />;
  }
}
export default connect()(AuthForm)