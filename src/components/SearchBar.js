/**
 * Created by joram on 1/9/18.
 */

import React, {Component} from 'react'

class SearchBar extends Component {
    render () {
        var style = {
            searchBar: {
                padding: " 0.13rem 0.75rem",
                borderRadius: "0.2rem",
                background: "#f8f9fa",
                textAlign: "center"
            }
        }
        return (
            <div className="search-bar">
                <form className="form-inline">
                    <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" style={style.searchBar} />
                </form>
            </div>
        )
    }
}

export default SearchBar;