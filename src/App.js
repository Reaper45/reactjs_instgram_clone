import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { connect } from 'react-redux'

import './assets/css/App.css';
import Home from './view/Home';
import Explore from './view/Explore';
import Activities from './view/Activities';
import Profile from './view/Profile';
import Welcome from './view/Welcome';
import NavBar from "./components/partials/NavBar";
import AuthForm from "./components/auth/AuthForm";
import Login from "./view/Login";

class App extends Component {
  render() {
    let user = this.props.user
    return (
      <Router>
        <div className="App bg-light">
          {user ? (<NavBar />) : null}
          <main>
            <Route exact path="/" component={user ? Home : Welcome} />
            <Route path="/accounts/signup/" component={AuthForm}/>
            <Route path="/accounts/login/" component={Login}/>
            <Route path="/explore" component={Explore}/>
            <Route path="/activities" component={Activities}/>
            <Route path="/profile" component={Profile}/>
          </main>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return state
}

export default connect(mapStateToProps)(App);
