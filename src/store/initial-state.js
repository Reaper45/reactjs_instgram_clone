/**
 * Created by joram on 2/16/18.
 */

export const initialState = {
  user: null,
  posts: [],
  errors: null
}