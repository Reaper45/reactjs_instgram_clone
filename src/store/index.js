/**
 * Created by joram on 2/16/18.
 */

import {createStore, applyMiddleware} from 'redux'
import allReducers from './reducers'
import {initialState} from './initial-state'
import thunk from 'redux-thunk'

export default createStore(
  allReducers,
  initialState,
  applyMiddleware(thunk),
  window.devToolsExtension && window.devToolsExtension()
)
