/**
 * Created by joram on 2/27/18.
 */
import { SIGNUP_USER, LOGIN_USER } from '../actions/user-actions'
import { initialState } from '../initial-state.js'
import { setUser } from '../../utils'

export default function userReducer (state = initialState.user, action) {
  switch (action.type) {
    case SIGNUP_USER:
      setUser(action.payload)
      return Object.assign({}, state, action.payload)
    case LOGIN_USER:
      setUser(state)
      return Object.assign({}, state, action.payload)
    default:
      return state
  }
  return state
}