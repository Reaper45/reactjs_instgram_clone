/**
 * Created by joram on 2/16/18.
 */
import {initialState} from '../initial-state';
import { API_ERROR } from '../actions/post-actions'

import { combineReducers } from 'redux'
import postsReducer from './posts-reducer'
import userReducer from './user-reducer'

const errors = (state = initialState.errors, action) => {
  switch (action.type){
    case API_ERROR:
      return action.payload
    default:
      return state
  }
}

export default combineReducers({
  posts: postsReducer,
  user: userReducer,
  errors: errors
})
