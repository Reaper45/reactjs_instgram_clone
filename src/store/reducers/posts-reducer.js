/**
 * Created by joram on 2/16/18.
 */
import {initialState} from '../initial-state';
import { RECEIVE_POSTS, POST_COMMENT } from '../actions/post-actions';

export default function postsReducer (state = initialState.posts, action) {
  // let newState;
  switch (action.type){
    case RECEIVE_POSTS:
      return [...action.payload]
    case POST_COMMENT:
      return action.payload;
    default:
      return state;
  }
}