/**
 * Created by joram on 2/16/18.
 */
import fetch from "cross-fetch";
const url = "https://gist.githubusercontent.com/Reaper45/60a5106eb003fc735fabe306d1873df6/raw/61d6f298228caeb601717519b3d8430ab1bfece2/instagram.json";

export const FETCH_POSTS = 'FETCH_POSTS'
export const RECEIVE_POSTS = 'RECEIVE_POSTS'
export const POST_COMMENT = 'POST_COMMENT'
export const API_ERROR = 'API_ERROR'

export function fetchPosts () {
  return dispatch => {
    return fetch(url)
      .then(response => response.json())
      .then(json => dispatch(receivePosts(json.data)))
  }
}

export const postComment = data => {
  return {
    type: POST_COMMENT,
    payload: {...data}
  }
}

const receivePosts = posts => {
  return {
    type: RECEIVE_POSTS,
    payload: posts
  }
}