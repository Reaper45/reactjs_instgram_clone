/**
 * Created by joram on 2/27/18.
 */
export const CHANGE_USER = 'CHANGE_USER'
export const changeUser = newUser => {
  return {
    type: CHANGE_USER,
    payload: {
      id: 0,
      name: newUser
    }
  }
}

export const LOGIN_USER = 'SIGNUP_USER'
export const login = user => {
  return {
    type: SIGNUP_USER,
    payload: user
  }
}

export const SIGNUP_USER = 'SIGNUP_USER'
export const signup = user => {
  return {
    type: SIGNUP_USER,
    payload: user
  }
}
