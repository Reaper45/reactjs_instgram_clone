import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import registerServiceWorker from './registerServiceWorker';

import './assets/css/index.css';
import './assets/js/custom.js';
import './assets/js/bootstrap.js';
import App from './App';
import store from './store'

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
